package ma.demo.test;

import ma.demo.beans.Lieu;
import ma.demo.util.HibernateUtil;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		Lieu lieu = new Lieu();
		lieu.setNom("Salle 12");
		lieu.setAdresse("12, Rue Hassan II, Marrakech");
		session.save(lieu);
		tx.commit();
		session.close();
	}

}

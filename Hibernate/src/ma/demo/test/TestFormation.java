package ma.demo.test;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.Transaction;

import ma.demo.beans.Formation;
import ma.demo.beans.Lieu;
import ma.demo.util.HibernateUtil;

public class TestFormation {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		Lieu lieu =  (Lieu) session.get(Lieu.class, 1);
		Formation formation = new Formation();
		formation.setTheme("Java");
		formation.setDate(new Date());
		formation.setLieu(lieu);
		session.save(formation);
		tx.commit();
		session.close();
	}

}
